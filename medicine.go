package medicine

import (
	"database/sql"
	"strconv"
)

type MedicineDB struct {
	db *sql.DB
}

type ResultStatus struct {
	ID    string
	Error error
}

type ResultStatuses map[string]*ResultStatus

func Open(db *sql.DB) (*MedicineDB, error) {
	var medDB MedicineDB
	medDB.db = db
	return &medDB, nil
}

func (this *MedicineDB) GetMedicines() (Medicines, error) {
	rows, err := this.db.Query("SELECT id, name, description, quantity FROM medicines;")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	medicines := make(Medicines)

	for rows.Next() {
		var (
			id          int
			name        string
			description string
			quantity    float32
		)

		err = rows.Scan(&id, &name, &description, &quantity)
		if err != nil {
			return nil, err
		}
		medicine := &Medicine{
			id,
			name,
			description,
			quantity,
		}

		medicines[id] = medicine
	}
	return medicines, nil
}

func (this *MedicineDB) GetMedicine(id int) (*Medicine, error) {
	rows, err := this.db.Query("SELECT id, name, description, quantity FROM medicines WHERE id = ?;", id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var medicine *Medicine

	for rows.Next() {
		var (
			id          int
			name        string
			description string
			quantity    float32
		)
		err = rows.Scan(&id, &name, &description, &quantity)
		if err != nil {
			return nil, err
		}

		medicine = &Medicine{
			id,
			name,
			description,
			quantity,
		}
		break
	}
	return medicine, nil
}

func (this *MedicineDB) CUMedicines(meds []Medicine) ResultStatuses {
	results := make(ResultStatuses)
	newCount := 0
	for _, med := range meds {
		if med.ID <= 0 {
			var result *ResultStatus
			var keyID string
			newID, err := this.CreateMedicine(&med)
			if err != nil {
				keyID = "new-" + strconv.Itoa(newCount)
				result = &ResultStatus{
					keyID,
					err,
				}
				newCount = newCount + 1
			} else {
				keyID = strconv.Itoa(newID)
				result = &ResultStatus{
					keyID,
					nil,
				}
			}
			results[keyID] = result
		} else {
			err := this.UpdateMedicine(&med)
			if err != nil {
				result := &ResultStatus{
					strconv.Itoa(med.ID),
					err,
				}
				results[strconv.Itoa(med.ID)] = result
			} else {
				result := &ResultStatus{
					strconv.Itoa(med.ID),
					nil,
				}
				results[strconv.Itoa(med.ID)] = result
			}
		}
	}
	return results
}

func (this *MedicineDB) CreateMedicine(med *Medicine) (int, error) {
	result, err := this.db.Exec("INSERT INTO medicine (medicine_name, medicine_description) VALUES (?, ?);", med.Name, med.Description)
	if err != nil {
		return 0, err
	}
	returnID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(returnID), nil
}

func (this *MedicineDB) UpdateMedicine(med *Medicine) error {
	_, err := this.db.Exec("UPDATE CR_Medical.medicine SET medicine_name = ?, medicine_description = ? WHERE medicine_id = ?;", med.Name, med.Description, med.ID)
	if err != nil {
		return err
	}
	return nil
}

func (this *MedicineDB) DeleteMedicine(id int) error {
	_, err := this.db.Exec("DELETE FROM CR_Medical.medicine WHERE medicine_id = ?;", id)
	if err != nil {
		return err
	}
	return nil
}
