package medicine

type Medicine struct {
	ID          int     `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Quantity    float32 `json:"quantity"`
}

type Medicines map[int]*Medicine
