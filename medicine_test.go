package medicine

import (
	"database/sql"
	"fmt"
	"strconv"
	"testing"

	_ "github.com/go-sql-driver/mysql"
)

func TestGetMedicines(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical")
	if err != nil {
		t.Error(err)
	}

	medDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}
	medicines, err := medDB.GetMedicines()
	if err != nil {
		t.Error(err)
	}
	if medicines == nil {
		t.Log("No medicines returned from the GetMedicines() method call")
		t.Fail()
	}
}

func TestGetMedicine(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical")
	if err != nil {
		t.Error(err)
	}

	medDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	medID := 0

	medicine, err := GetFirstAvailableMedicine(medDB)
	if err != nil {
		t.Error(err)
	}

	medID = medicine.ID

	if medID > 0 {
		medicine, err := medDB.GetMedicine(medID)
		if err != nil {
			t.Error(err)
		}

		if medicine == nil {
			t.Log("Medicine result is nil")
			t.Fail()
		}
		if medicine.ID != medID {

			t.Logf("Medicine id is not equal to what was provided. expected: %v, got: %v", medID, medicine.ID)
			t.Fail()
		}
	} else {
		t.Log("Unable to run GetMedicine test. No records exist.")
		t.Fail()
	}
}

func TestCreateMedicine(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical")
	if err != nil {
		t.Error(err)
	}

	medDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	medicine := &Medicine{
		0,
		"Created Medicine",
		"This is a new test medicine",
		0,
	}

	newID, err := medDB.CreateMedicine(medicine)
	if err != nil {
		t.Error(err)
	}

	newMedicine, err := medDB.GetMedicine(newID)
	if err != nil {
		t.Error(err)
	}

	if newMedicine == nil {
		t.Log("Medicine insert did not return the correct medicine id")
		t.Fail()
	}

	if newMedicine.ID != newID {
		t.Log("Medicine ID is not the same")
		t.Fail()
	}

	if newMedicine.Name != medicine.Name {
		t.Log("Medicine Name is not the same")
		t.Fail()
	}

	if newMedicine.Description != medicine.Description {
		t.Log("Medicine Description is not the same")
		t.Fail()
	}

	if newMedicine.Quantity != 0 {
		t.Log("Medicine Quantity is not zero")
		t.Fail()
	}
}

func TestUpdateMedicine(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical")
	if err != nil {
		t.Error(err)
	}

	medDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	medicine, err := GetFirstAvailableMedicine(medDB)
	if err != nil {
		t.Error(err)
	}

	newName := "Updated Medicine"
	newDescription := "This is an updated test medicine."

	medicine.Name = newName
	medicine.Description = newDescription

	err = medDB.UpdateMedicine(medicine)
	if err != nil {
		t.Error(err)
	}

	newMedicine, err := medDB.GetMedicine(medicine.ID)
	if err != nil {
		t.Error(err)
	}

	if newMedicine.ID != medicine.ID {
		t.Log("Medicine ID has changed when it was not supposed to")
		t.Fail()
	}

	if newMedicine.Name != medicine.Name {
		t.Log("Medicine Name has changed when it was not supposed to")
		t.Fail()
	}

	if newMedicine.Description != medicine.Description {
		t.Log("Medicine Description has changed when it was not supposed to")
		t.Fail()
	}

	if newMedicine.Quantity != medicine.Quantity {
		t.Log("Medicine Quantity has changed when it was not supposed to")
		t.Fail()
	}
}

func TestCUMedicines(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical")
	if err != nil {
		t.Error(err)
	}

	medDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	medicines := make([]Medicine, 2)

	medicine1 := Medicine{
		0,
		"Created Medicine",
		"This is a new test medicine",
		0,
	}

	medicine2, err := GetFirstAvailableMedicine(medDB)
	if err != nil {
		t.Error(err)
	}

	medicine2.Name = "Updated Medicine"
	medicine2.Description = "This is an updated test medicine."

	medicines = append(medicines, medicine1)
	medicines = append(medicines, *medicine2)

	results := medDB.CUMedicines(medicines)

	for _, value := range results {
		if value.Error != nil {
			t.Error(value.Error)
		} else {
			id, err := strconv.Atoi(value.ID)
			if err != nil {
				t.Error(err)
			}
			if id <= 0 {
				t.Log("NewID is less than or equal to zero")
				t.Fail()
			}
		}
	}
}

func TestDeleteMedicine(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical")
	if err != nil {
		t.Error(err)
	}

	medDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	tempMedicine, err := GetFirstAvailableMedicine(medDB)
	if err != nil {
		t.Error(err)
	}

	id := tempMedicine.ID

	medDB.DeleteMedicine(id)

	medicine, err := medDB.GetMedicine(id)
	if err != nil {
		t.Error(err)
	}
	if medicine != nil {
		t.Log("medicine delete failed")
		t.Fail()
	}
}

func GetFirstAvailableMedicine(medDB *MedicineDB) (*Medicine, error) {
	medList, err := medDB.GetMedicines()
	if err != nil {
		return nil, err
	}

	if len(medList) <= 0 {
		return nil, fmt.Errorf("No available medicines to return")
	}

	var medicine *Medicine

	for _, value := range medList {
		medicine = value
		break
	}
	return medicine, nil
}
