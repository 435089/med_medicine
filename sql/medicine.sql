drop database CR_Medical;
drop user medical@localhost;
FLUSH PRIVILEGES;

create database CR_Medical;
use CR_Medical;

create user 'medical'@'localhost' IDENTIFIED BY 'gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1';

create table medicine(
    medicine_id INT NOT NULL AUTO_INCREMENT,
    medicine_name VARCHAR(100) NOT NULL,
    medicine_description VARCHAR(250),
    PRIMARY KEY (medicine_id)
);

create table medicine_ledger(
    transaction_id INT NOT NULL AUTO_INCREMENT,
    medicine_id INT NOT NULL,
    amount DECIMAL(10,1) NOT NULL,
    transaction_date DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (transaction_id),
    CONSTRAINT fk_medicine_id
    FOREIGN KEY (medicine_id)
    REFERENCES medicine(medicine_id)
);

 CREATE TRIGGER before_medicine_delete
 BEFORE DELETE ON medicine
 FOR EACH ROW
 DELETE FROM medicine_ledger WHERE medicine_id = OLD.medicine_ID;

CREATE VIEW medicines AS
select 
 m.medicine_id 'id',
 m.medicine_name 'name', 
 m.medicine_description 'description', 
 IFNULL(SUM(mt.amount), 0) quantity 
 from medicine m 
 LEFT JOIN medicine_ledger mt 
 ON mt.medicine_id = m.medicine_id 
 GROUP BY m.medicine_id;


 GRANT SELECT, INSERT, UPDATE, DELETE ON CR_Medical.medicine TO 'medical'@'localhost';
 GRANT SELECT, INSERT, UPDATE, DELETE ON CR_Medical.medicines TO 'medical'@'localhost';
 GRANT SELECT, INSERT, UPDATE, DELETE ON CR_Medical.medicine_ledger TO 'medical'@'localhost';

 INSERT INTO medicine (medicine_name, medicine_description) VALUES ('First Test Medicine', 'This is the first test medicine');
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (1, 20);
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (1, 13.5);
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (1, 45);

 INSERT INTO medicine (medicine_name, medicine_description) VALUES ('Second Test Medicine', 'This is the second test medicine');
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (2, 12);
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (2, 19);
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (2, 2);

 INSERT INTO medicine (medicine_name, medicine_description) VALUES ('Third Test Medicine', 'This is the third test medicine');
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (3, 15);
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (3, 20);
 INSERT INTO medicine_ledger (medicine_id, amount) VALUES (3, 5);
